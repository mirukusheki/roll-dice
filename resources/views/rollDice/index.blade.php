<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Javascript Roll Dice</title>
</head>
<body>
<h2>JavaScript Roll Dice</h2>

<p>Let's Roll Dice!</p>

<button onclick="roll()">Roll Dice!</button>
Times roll: <span id="roll"></span>
<br>
<p>First Dice: <span id="demo"></span></p>
<p>Second Dice: <span id="demo2"></span></p>
<p>Sum: <span id="total"></span></p>
<br>
<p>Times Doubled: <span id="doubled"></span></p>

<script>
  var rollfirstdice  = 0;
  var rollseconddice = 0;
  var total          = rollfirstdice + rollseconddice;
  var double         = "";
  var timesRoll      = 0;
  var timesDoubled   = 0;

  document.getElementById("demo").innerHTML    = rollfirstdice;
  document.getElementById("demo2").innerHTML   = rollseconddice;
  document.getElementById("total").innerHTML   = total + double;
  document.getElementById("roll").innerHTML    = timesRoll;
  document.getElementById("doubled").innerHTML = timesDoubled;

  function roll() {
    var rollfirstdice  = Math.floor(Math.random() * 6) + 1;
    var rollseconddice = Math.floor(Math.random() * 6) + 1;
    var total          = rollfirstdice + rollseconddice;
    var double         = "";
    if (rollfirstdice == rollseconddice) {
      newTimesDoubled = timesDoubled + 1;
      timesDoubled    = newTimesDoubled
      var double      = " (It's double!)";
    }
    newTimesRoll = timesRoll + 1;
    timesRoll    = newTimesRoll;

    document.getElementById("demo").innerHTML    = rollfirstdice;
    document.getElementById("demo2").innerHTML   = rollseconddice;
    document.getElementById("total").innerHTML   = total + double;
    document.getElementById("roll").innerHTML    = timesRoll;
    document.getElementById("doubled").innerHTML = timesDoubled;
  }
</script>

</body>
</html>